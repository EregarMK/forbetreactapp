import React from 'react';
import './App.css';

const API = 'https://www.iforbet.pl/rest/market/categories/multi/2432/events';

class App extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            events: []
        }
    }

    componentDidMount() {
        fetch(API)
            .then(response => response.json())
            .then(data => {
                let events = data.data.map((event) => {
                    return(
                        <div className='fb-event-tile'>
                            {event.remoteId == 0 &&
                                <b>{event.category1Name} / {event.category2Name} / {event.category3Name}</b>
                            }
                            <div className='fb-event-title' key={event.eventId}>{event.eventName}</div>
                            {event.eventGames[0].outcomes.map((outcome) => {
                                return (
                                    <div className='fb-event-outcome' key={outcome.outcomeId}>{outcome.outcomeName} <span>{outcome.outcomeOdds}</span></div>
                                )
                            })}
                        </div>
                    )
                })
                
                this.setState({ events: events })
            
            })
        .catch(error => {
            console.log("There is a problem with api");
        })
    }

    render() {
        const { events } = this.state;
        return (
            <div className='fb-events-container'>
                {events}
            </div>
        )
    }
}

export default App